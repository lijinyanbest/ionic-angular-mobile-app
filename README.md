Ionic Angular Mobile App (v1.0.0)
==========
In this demo, I implement mobile App with Ionic/Angular 9 with Cordova which will support both IOS and Android. The App will allow user to register and login. After sign in,  user can add, edit, update and delete a item. Chart.js is imported to build chart views inside the application. User can only view the item list with out signin. 

Run and test the app on multiple screen sizes and platform types: 
==========
```
$ ionic serve --lab
```

Ionic Cordova add platform IOS and Android
==========
```
$ ionic cordova platform add ios
$ ionic cordova platform add android
```

This User Interface uses HttpClientModule to call REST web service:
==========
https://gitlab.com/lijinyanbest/spring-boot-apis.git

| Methods | URLs |Actions|
| ------ | ------ |------ |
| POST	 | /api/auth/signup |signup new account |
| POST	 | /api/auth/signin |login an account |
| POST	 | /api/tutorial |create new tutorial |
| GET	 | /api/tutorial |retrieve all tutorials |
| GET	 | /api/tutorial/:id |retrieve a tutorial by :id |
| PUT	 | /api/tutorial/:id |update a tutorial by :id |
| DELETE | /api/tutorial/:id |delete a Tutorial by :id |
| DELETE | /api/tutorial |delete all tutorial |
| GET    | /api/tutorials?title=[keyword]|find all Tutorials which title contains keyword |





Screenshots of mobile web Appliation:
==========
| Login page with validation in IOS and Android simulator| Register page with validation in IOS and Android simulator|
| ------ | ------ |
| <img src="/src/assets/images/loginIphong.png" width="200px" height="400px" align="left"><img src="/src/assets/images/loginAndroid.png"  width="200px" height="400px" align="right"> |<img src="/src/assets/images/registerIphone.png" width="200px" height="400px" align="left"><img src="/src/assets/images/registerAndroid.png" width="200px" height="400px" align="right"> |


| Home page before signin| Home and chart page after signin  |
| ------ | ------ |
| <img src="/src/assets/images/homepagelab.png" width="400px" align="left"> | <img src="/src/assets/images/homesignin.png" width="200px" align="left"><img src="/src/assets/images/chart.png" width="200px" align="left"> |

| Menu page before signin | Menu page after signin |
| ------ | ------ |
| <img src="/src/assets/images/menulab.png" width="500px" align="left"> | <img src="/src/assets/images/menuSignin.png" width="250px" align="left"> |


| Profile page after signin | Add and edit page after signin |
| ------ | ------ |
| <img src="/src/assets/images/profilesignin.png" width="300px" align="left">  | <img src="/src/assets/images/edit.png" width="250px" align="left"><img src="/src/assets/images/add.png" width="250px" align="left">  |

<br><br><br><br><br><br><br>
