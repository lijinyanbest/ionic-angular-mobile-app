import { Component, OnInit } from '@angular/core';
import { VideoPlayer, VideoOptions } from '@ionic-native/video-player/ngx';


@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {

  videoOptions: VideoOptions

  constructor(
    private videoPlayer: VideoPlayer
  ) { 
    this.videoOptions = {
      volume:0.7
    }
  }

  ngOnInit() {
  }

  playOfflineVideo() {
    this.videoPlayer.play('file:///android_asset/www/movie.mp4').then(() => {
      console.log('video finished');
    }).catch(error => {
      console.log(error);
    });
  }

  playOnlineVideo() {
    this.videoPlayer.play('http://static.videogular.com/assets/videos/elephants-dream.mp4').then(() => {
      console.log('video finished');
    }).catch(error => {
      console.log(error);
    });
  }

  closeVideoPlayer() {
    this.videoPlayer.close();
  }

}
