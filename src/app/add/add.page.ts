import { Component, NgZone, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { TutorialService } from '../shared/tutorial.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {
  tutorialForm:FormGroup;
  isSubmitted = false;

  constructor(
    private tutorialService:TutorialService,
    private router:Router,
    private fb: FormBuilder,
    private zone:NgZone
    ) { 
      
    }

  ngOnInit() {
    this.tutorialForm = this.fb.group({
      title: ['', [Validators.required, Validators.minLength(4)]],
      description:['', [Validators.required, Validators.minLength(4)]]
    })
  }

  get errorControl(){
      return this.tutorialForm.controls;
  }

  onFormSubmit(){
    this.isSubmitted = true;

    if(!this.tutorialForm.valid){
      return false;
    }else{
      this.tutorialService.create(this.tutorialForm.value)
        .subscribe(res => {
          this.zone.run(() => {
            this.tutorialForm.reset();
            this.router.navigate(['/home']);
          })
        });
    }
     
  }

}
