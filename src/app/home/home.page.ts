import { Component, OnInit } from '@angular/core';

import { TutorialService } from '../shared/tutorial.service';

import { TokenStorageService } from '../shared/token-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  isLoggedIn = false;

  constructor(private service: TutorialService, private tokenService: TokenStorageService) {}

  tutorials: any = [];
  ngOnInit() { 
    this.isLoggedIn = !!this.tokenService.getToken();
  }

  ionViewDidEnter(){
    this.service.getAll().subscribe(res => {
      this.tutorials = res;
    });
  }

  deletetutorial(tutorial, i){
    if(window.confirm(" Do you want to delete the Tutorial?")){
      this.service.delete(tutorial.id).subscribe(() => {
        this.tutorials.splice(i,1);
        
      });
    }
  }

}
