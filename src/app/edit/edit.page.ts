import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TutorialService } from '../shared/tutorial.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.page.html',
  styleUrls: ['./edit.page.scss'],
})
export class EditPage implements OnInit {

  updateForm: FormGroup;
  id:any;

  constructor(
    private service: TutorialService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.id = this.actRoute.snapshot.paramMap.get("id");
   }

  ngOnInit() {
    this.getTutorialdata(this.id);
    this.updateForm = this.fb.group({
      title: [''],
      description: [''],
      published:['']
    })
  }

  getTutorialdata(id){
    this.service.get(id).subscribe(res => {
      this.updateForm.setValue({
        title:res['title'],
        description:res['description'],
        published:res['published']
      });
    });
  }

  updatetutorialForm() {
    if(!this.updateForm.valid){
      return false;
    }else{
      this.service.update(this.id, this.updateForm.value)
        .subscribe(res => {
          this.updateForm.reset();
          this.router.navigate(['/home']);
        });
    }
  }

}
