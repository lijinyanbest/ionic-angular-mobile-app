import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';


const baseUrl="http://192.168.1.78:8080/api/tutorials";

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  constructor(private http: HttpClient) { }

  getAll():Observable<any>{
    return this.http.get(baseUrl);
  }

  get(id):Observable<any>{
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data):Observable<any>{
    return this.http.post(baseUrl,data);
  }

  update(id, data):Observable<any>{
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  delete(id):Observable<any>{
    return this.http.delete(`${baseUrl}/${id}`);
  }

  deleteAll():Observable<any>{
    return this.http.delete(baseUrl);
  }

  getPublished():Observable<any>{
    return this.http.get(baseUrl+"/published");
  }

  findByTitle(title):Observable<any>{
    return this.http.get(`${baseUrl}/${title}`);
  }
}
