
import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../shared/auth.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerform:FormGroup;

  isSubmitted = false;
  isSuccessful = false;
  isSignUpFailed = false;

  errorMessage="";


  constructor(private authService: AuthService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerform = this.formBuilder.group({
        username:['', [Validators.required, Validators.minLength(4)]],
        email:['', [Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')]],
        password:['', [Validators.required, Validators.pattern('[0-9a-zA-Z ]*')]]
    });
  }

  get errorControl() {
    return this.registerform.controls;
  }


  register() {
     this.isSubmitted  = true;
     if(!this.registerform.valid){
       return false;
     }else{
       this.authService.register(this.registerform.value).subscribe(
         data => {
           console.log(data);
           this.isSuccessful = true;
          this.isSignUpFailed = false;
         }, err => {
           this.errorMessage = err.error.message;
           console.log("error: " + err.error.message);
           this.isSignUpFailed = true;
         }
       );


     }

  }

}
