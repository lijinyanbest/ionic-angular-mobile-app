import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/app/shared/auth.service';
import { TokenStorageService } from '../../shared/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  isLoginFailed = false;
  isLoggedIn = false;
  errorMessage = '';

  constructor(private authService: AuthService,
    private tokenService: TokenStorageService,
    private formbuild: FormBuilder,
    private navCtrl: NavController,
    private menuCtrl:MenuController,
    public toastCtrl: ToastController,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private router: Router
    ) {

     }

  ngOnInit() {

    this.loginForm = this.formbuild.group({
      'username': [null, Validators.compose([
        Validators.required
      ])],
      'password': [null, Validators.compose([
        Validators.required
      ])]
    });


    if (this.tokenService.getToken()) {
      this.isLoggedIn = true;
    }
  }

  login(){

    if (!this.loginForm.valid){
      return false;
    }else{

      this.authService.login(this.loginForm.value).subscribe(
        data => {
          this.tokenService.saveToken(data.accessToken);
          this.tokenService.saveUser(data);
          this.reloadPage();
        },err => {
          this.errorMessage = err.error.message;
          this.isLoginFailed = true;
        }
      );
    }

  }

  reloadPage() {
    window.location.reload();
  }

 async forgotPass() {
    const alert = await this.alertCtrl.create({
      header: 'Forgot Password?',
      message: 'Enter you email address to send a reset link password.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'Email'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirm',
          handler: async () => {
            const loader = await this.loadingCtrl.create({
              duration: 2000
            });

            loader.present();
            loader.onWillDismiss().then(async l => {
              const toast = await this.toastCtrl.create({
                message: 'Email was sended successfully.',
                duration: 3000,
                position: 'bottom'
              });

              toast.present();
            });
          }
        }
      ]
    });

    await alert.present();
  }

}
