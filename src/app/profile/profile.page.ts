import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from '../shared/token-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  currentUser: any;

  constructor(private tokenService: TokenStorageService) { }

  ngOnInit() {
    this.currentUser = this.tokenService.getUser();
  }

}
